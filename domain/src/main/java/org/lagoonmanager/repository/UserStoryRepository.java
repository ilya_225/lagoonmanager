package org.lagoonmanager.repository;

import org.lagoonmanager.entity.UserStory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserStoryRepository extends CrudRepository<UserStory, Long> {

    List<UserStory> findAll();
}
