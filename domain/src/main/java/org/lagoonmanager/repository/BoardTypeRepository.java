package org.lagoonmanager.repository;

import org.lagoonmanager.entity.BoardType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface BoardTypeRepository extends JpaRepository<BoardType, Long> {
    BoardType findByName(String name);

    @Query("SELECT projframe FROM BoardType projframe ORDER BY projframe.name")
    List<BoardType> findProjectFrameworks();
}
