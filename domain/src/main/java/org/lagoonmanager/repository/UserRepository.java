package org.lagoonmanager.repository;

import org.lagoonmanager.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {


    User findByEmail(String email);

    @Transactional
    User findByUsername(String username);

    List<User> findAll();
}