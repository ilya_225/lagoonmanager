package org.lagoonmanager.repository;

import org.lagoonmanager.entity.Project;
import org.lagoonmanager.entity.User;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends BaseRepository<Project, Long> {

    Optional<Project> findOne(Long id);

    List<Project> findByOwner(User owner);

    Integer countByOwnerAndId(User owner, Long id);

}
