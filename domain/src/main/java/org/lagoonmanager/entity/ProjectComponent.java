package org.lagoonmanager.entity;

import javax.persistence.*;

@Entity
@Table(name = "project_components")
public class ProjectComponent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "project_layer_id",
        foreignKey = @ForeignKey(name = "PROJECT_LAYER_ID")
    )
    private ProjectLayer projectLayer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProjectLayer getProjectLayer() {
        return projectLayer;
    }

    public void setProjectLayer(ProjectLayer projectLayer) {
        this.projectLayer = projectLayer;
    }
}
