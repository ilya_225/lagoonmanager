package org.lagoonmanager.service;

import org.lagoonmanager.entity.User;
import org.lagoonmanager.form.UserForm;

import java.util.List;

public interface UserService {

    void save(UserForm userForm);

    List<User> findUsersByUsername();
}
