package org.lagoonmanager.service;

import org.lagoonmanager.entity.Project;
import org.lagoonmanager.entity.User;
import org.lagoonmanager.entity.UserStory;
import org.lagoonmanager.form.ProjectForm;

import java.util.List;

public interface ProjectService {

    Project saveProject(ProjectForm projectForm);

    List<Project> findByOwner();

    Project findProject(Long id);

    Integer countUserProject(User user, Long id);

    UserStory saveUserStory(UserStory userStory);

    List<UserStory> findAllUserStories();
}
