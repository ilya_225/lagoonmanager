package org.lagoonmanager.service.impl;

import org.lagoonmanager.form.ProjectForm;
import org.lagoonmanager.form.UserForm;
import org.lagoonmanager.service.WebFormFactory;
import org.springframework.stereotype.Service;

@Service
public class WebFormFactoryImpl implements WebFormFactory {

    @Override
    public UserForm createUserForm() {
        return new UserForm();
    }

    @Override
    public ProjectForm createProjectForm() {
        return new ProjectForm();
    }
}
