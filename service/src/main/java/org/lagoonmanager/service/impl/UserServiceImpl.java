package org.lagoonmanager.service.impl;

import org.lagoonmanager.entity.Role;
import org.lagoonmanager.entity.User;
import org.lagoonmanager.form.UserForm;
import org.lagoonmanager.repository.RoleRepository;
import org.lagoonmanager.repository.UserRepository;
import org.lagoonmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void save(UserForm userForm) {
        Role role = roleRepository.findByRole("ROLE_USER");
        if (role == null) {
            role = new Role("ROLE_USER");
            roleRepository.save(role);
        }
        User user = new User();
        user.setUsername(userForm.getUsername());
        user.setEmail(userForm.getEmail());
        user.setPassword(passwordEncoder.encode(userForm.getPassword()));
        user.addRole(role);
        user.setEnabled(true);
        userRepository.save(user);

    }

    @Override
    public List<User> findUsersByUsername() {
        return userRepository.findAll();
    }
}
