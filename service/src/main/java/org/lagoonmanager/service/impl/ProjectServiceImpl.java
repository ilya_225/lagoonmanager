package org.lagoonmanager.service.impl;

import org.lagoonmanager.entity.Project;
import org.lagoonmanager.entity.User;
import org.lagoonmanager.entity.UserStory;
import org.lagoonmanager.exception.RepositoryEmptyResultException;
import org.lagoonmanager.form.ProjectForm;
import org.lagoonmanager.repository.ProjectRepository;
import org.lagoonmanager.repository.UserStoryRepository;
import org.lagoonmanager.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository projectRepository;
    private UserStoryRepository userStoryRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository,
                              UserStoryRepository userStoryRepository) {
        this.projectRepository = projectRepository;
        this.userStoryRepository = userStoryRepository;
    }

    @Override
    public Project saveProject(ProjectForm projectForm) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Project project = new Project();
        project.setOwner(user);
        project.setPublic(projectForm.getVisibility());
        project.setDescription(projectForm.getDescription());
        project.setName(projectForm.getTitle());

        projectRepository.save(project);

        return project;
    }

    @Override
    public List<Project> findByOwner() {

        User owner = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return projectRepository.findByOwner(owner);
    }

    @Override
    public Project findProject(Long id) {

        Optional<Project> projectOptional = projectRepository.findOne(id);

        Project project = projectOptional.orElseThrow(
                () -> new RepositoryEmptyResultException("projectRepository", "findOne")
        );
        //TODO lazy load check
        project.getOwner().getId();

        return project;

    }

    @Override
    public UserStory saveUserStory(UserStory userStory) {
        userStoryRepository.save(userStory);

        return userStory;
    }

    @Override
    public List<UserStory> findAllUserStories() {
        return userStoryRepository.findAll();
    }

    @Override
    public Integer countUserProject(User user, Long id) {
        return projectRepository.countByOwnerAndId(user, id);
    }
}
