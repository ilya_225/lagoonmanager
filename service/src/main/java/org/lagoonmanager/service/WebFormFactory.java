package org.lagoonmanager.service;

import org.lagoonmanager.form.ProjectForm;
import org.lagoonmanager.form.UserForm;

public interface WebFormFactory {

    UserForm createUserForm();

    ProjectForm createProjectForm();
}
