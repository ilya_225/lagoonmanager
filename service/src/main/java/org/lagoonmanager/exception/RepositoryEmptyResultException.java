package org.lagoonmanager.exception;

public class RepositoryEmptyResultException extends RuntimeException {

    String repository;
    String reposytoryMethod;

    public RepositoryEmptyResultException(String repository, String repositoryMethod) {
        this.repository = repository;
        this.reposytoryMethod = repositoryMethod;
    }

    @Override
    public String toString() {
        return "repository " + this.repository + "\n" + "method " + this.reposytoryMethod + "\n";
    }
}
