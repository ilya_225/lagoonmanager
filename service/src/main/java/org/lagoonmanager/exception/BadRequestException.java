package org.lagoonmanager.exception;


public class BadRequestException extends RuntimeException {

    public BadRequestException(){}

    public BadRequestException(String url) {
        super(url);
    }
}
