package org.lagoonmanager.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(){}

    public NotFoundException(String url) {
        super(url);
    }
}
