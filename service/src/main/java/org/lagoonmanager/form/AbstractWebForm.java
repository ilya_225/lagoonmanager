package org.lagoonmanager.form;


import java.lang.reflect.Field;

public abstract class AbstractWebForm {

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        Class c = this.getClass();
        Field[] fields = c.getDeclaredFields();
        for( Field field: fields) {
            field.setAccessible(true);
            try {
                result.append(field.getName());
                result.append(": ");
                result.append(field.get(this));
                result.append("\n");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return result.toString();
    }
}
