package org.lagoonmanager.form;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProjectForm extends AbstractWebForm {

    @Size(min=1,max = 255)
    @NotNull
    private String title;

    @NotNull
    private String description;

    private Boolean visibility = false;

    public ProjectForm() {
    }

    public ProjectForm(String title, String description, Boolean visibility) {
        this.title = title;
        this.description = description;
        this.visibility = visibility;
    }

    public ProjectForm(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        if(visibility.equals("on")) {
            this.visibility = true;
        }
    }
}
