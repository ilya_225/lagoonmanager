package org.lagoonmanager.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.lagoonmanager.constraint.UniqueEmail;

import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

public class UserForm extends AbstractWebForm {

    @Size(min=5,max=50)
    private String username;

    @Size(min=8,max=20)
    private String password;

    @Size(min=8,max=20)
    private String confirm;

    @Email
    @NotBlank
    @UniqueEmail
    private String email;

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
