package org.lagoonmanager.validator;

import org.lagoonmanager.form.UserForm;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Service
public class PasswordValidator implements Validator {

    public boolean supports(Class c) {
        return UserForm.class.isAssignableFrom(c);
    }

    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmptyOrWhitespace(e,"password", "2", "Password is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(e, "confirm", "3", "Confirm field is required");

        UserForm userForm = (UserForm) obj;

        if(!userForm.getPassword().equals(userForm.getConfirm()))
        {
            e.rejectValue("password", "1","password and confirm not equal");
        }
    }
}
