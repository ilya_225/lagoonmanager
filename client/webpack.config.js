var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './src/app/entry.js',
  output: { path: __dirname + "/dist/static/js/", filename: 'bundle.js' },
  module: {
    loaders: [
      {
        test: /.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  watch: true
};
