window.helpers = {
    closest,
    getCoords,
    animate,
    animateColor,
    DragManager,
    emptyElement
}

function emptyElement(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function closest(el, selector) {
    var matchesFn;

    // find vendor prefix
    ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
        if (typeof document.body[fn] == 'function') {
            matchesFn = fn;
            return true;
        }
        return false;
    })

    var parent;

    // traverse parents
    while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
            return parent;
        }
        el = parent;
    }

    return null;
}

function getCoords(elem) {
    var box = elem.getBoundingClientRect();
  
    return {
      top: box.top + pageYOffset,
      left: box.left + pageXOffset
    };
  
}

function animate(object, property, start_value, end_value, time) {
    var frame_rate = 0.06;
    var frame = 0;
    var delta = (end_value - start_value) / time / frame_rate;
    var handle = setInterval(function() {
      frame++;
      var value = start_value + delta * frame;
      object.style[property] = value + "px";
      if (value == end_value) {
        clearInterval(handle);
      }
    }, 1 / frame_rate);
}

function animateColor(object, property, start_value, end_value, time) {
   // var hex = start_value
    console.log(0xfff);
    /*var frame_rate = 0.06;
    var frame = 0;
    var delta = (end_value - start_value) / time / frame_rate;
    var handle = setInterval(function() {
      frame++;
      var value = start_value + delta * frame;
      object.style[property] = value;
      if (value == end_value) {
        clearInterval(handle);
      }
    }, 1 / frame_rate);*/
}

/**
 * Create a new DragManager instance 
 * @constructor
 * @param {String} target - HTMLCollection or HTMLElement
 * @param {Object} options - { 
 *  mouseDownHandler : callback, 
 *  mouseMoveHandler : callback,
 *  mouseUpHandler   : callback,
 *  defaultHandlers  : boolean
 * }
 */
function DragManager(target, options = { defaultHandlers: true}) {
    const self = this;
    self.dragObject = {
        isDragging : false,
        shiftX: 0,
        shiftY: 0,
        element: null,
        parent: null,
        old: null,
        refresh: function() {
            this.isDragging = false;
            this.element = null;
            this.parent = null;
            this.shiftX = 0;
            this.shiftY = 0;
        },
        beginDrag: function(el, parent, x, y) {
            this.element = el;
            this.parent = parent;
            this.isDragging = true;
            this.setShifting(x,y);
        },
        setShifting: function(x,y) {
            this.shiftX = x - window.helpers.getCoords(this.element).left;
            this.shiftY = y - window.helpers.getCoords(this.element).top;
        },
    }

    function defaultMouseDownHandler(e, elem) {
        self.dragObject.refresh();
        const parent = elem.parentNode;
        self.dragObject.old = {
            zIndex: elem.style.zIndex || '',
            width: elem.style.width || '',
            position: elem.style.position || '',
            left: elem.style.left || '',
            top: elem.style.top || ''
        }
        self.dragObject.beginDrag(elem, parent, e.pageX, e.pageY);
        elem.style.position = "absolute";
        document.body.appendChild(elem);
        elem.style.zIndex = 9;
        
        defaultMouseMoveHandler(e)
    };
    function defaultMouseMoveHandler(e) {
        if(self.dragObject.isDragging) {
            self.dragObject.element.style.left = (e.pageX - self.dragObject.shiftX) + 'px';
            self.dragObject.element.style.top = (e.pageY - self.dragObject.shiftY)  + 'px';
        }
    };

    function defaultMouseUpHandler(e) {
        //TODO default mouseup handler
    }
    self.options = options;

    if(target instanceof HTMLCollection) {
        Array.prototype.forEach.call(target, (el) => {
            bindHandlers(el);
        });
    } else {
        bindHandlers(target);
    }

    document.body.addEventListener('mouseup', (e) => {
        if('mouseUpHandler' in options)
            options.mouseUpHandler(e);
        if(options.defaultHandlers)
            defaultMouseUpHandler(e);
    });
    document.body.addEventListener('mousemove', (e) => {
        if(options.defaultHandlers)
            defaultMouseMoveHandler(e);
        if('mouseMoveHandler' in options)
            options.mouseMoveHandler(e);
    });

    function bindHandlers(el) {
        el.addEventListener('dragstart', e => {
            e.preventDefault();
            return false;
         });
        el.addEventListener('mousedown', e => {
            if(options.defaultHandlers)
                defaultMouseDownHandler(e, el);
            if('mouseDownHandler' in options)
                options.mouseDownHandler(e);
        });
    }
}