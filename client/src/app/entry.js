import 'whatwg-fetch'
import './helpers'
import './components/board'
import './components/create_board'
import './components/general_page'
import './components/user_story'

window.onload = function() {
  //$(".brand-logo").sideNav();

  if(document.getElementById('editor')) {
    CKEDITOR.replace('editor');
  //formEditor();
  }
};