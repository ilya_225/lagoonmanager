if(document.getElementById('board-container')) {
    document.body.style.overflowX="hidden";
    const boardContainer = document.getElementById('board-container');
    const boardCards = document.getElementsByClassName('dragable-card');
    const boardSections = document.getElementsByClassName('board-section');
    const addButtons = document.getElementsByClassName('board-add-button');

    const currentForm = {
        form: null,
        section: null,
        createButton: null,
        clearForm: function() {
            if(this.section && this.form) {
                this.section.removeChild(this.form);
                if(this.createButton)
                    this.createButton.style.display="block";
                this.form = null;
                this.section = null;
                this.createButton = null;
            }
        }
    };

    const dragObject = {
        isDragging : false,
        shiftX: 0,
        shiftY: 0,
        element: null,
        parent: null,
        old: null,
        refresh: function() {
            this.isDragging = false;
            this.element = null;
            this.parent = null;
            this.shiftX = 0;
            this.shiftY = 0;
        },
        beginDrag: function(el, parent, x, y) {
            this.element = el;
            this.parent = parent;
            this.isDragging = true;
            this.setShifting(x,y);
        },
        setShifting: function(x,y) {
            this.shiftX = x - window.helpers.getCoords(this.element).left;
            this.shiftY = y - window.helpers.getCoords(this.element).top;
        },
    }

    const scrollObj = {
        clientX : 0,
        shift: 0,
        next: function(x) {
            if(this.clientX !== 0)
                this.shift = x - this.clientX;
            else
                this.shift = 0;
            this.clientX = x;
            return 15;
        },
        prev: function(x) {
            if(this.clientX !== 0)
                this.shift = x - this.clientX;
            else
                this.shift = 0;
            this.clientX = x;
            return -15;
        }
    }
    

    document.body.addEventListener('mouseup', mouseUpHandler);
    document.body.addEventListener('mousemove', mouseMoveHandler);

    Array.prototype.forEach.call(
        boardCards,
        el => {
            el.addEventListener('dragstart', dragStartHandler);
            el.addEventListener('mousedown', mouseDownHandler(el));
        }, 
        boardCards
    );

    function dragStartHandler(e) {
        e.preventDefault();
    }

    function mouseDownHandler(elem) {
        return function(e) {
            dragObject.refresh();
            const parent = elem.parentNode;
            dragObject.old = {
                zIndex: elem.style.zIndex || '',
                width: elem.style.width || '',
                position: elem.style.position || '',
                left: elem.style.left || '',
                top: elem.style.top || ''
            }
            dragObject.beginDrag(elem, parent, e.pageX, e.pageY);
            elem.style.position = "absolute";
            document.body.appendChild(elem);
            elem.style.zIndex = 9;
            
            mouseMoveHandler(e)
        }
    }

    function mouseMoveHandler(e) {
        if(dragObject.isDragging) {
            
            dragObject.element.style.left = (e.pageX - dragObject.shiftX) + 'px';
            dragObject.element.style.top = (e.pageY - dragObject.shiftY)  + 'px';

            //TODO this part also need to change (need to calculate correct offset)
            if(dragObject.element.getBoundingClientRect().left >= 
                (boardContainer.offsetWidth)) {
                    if(scrollObj.clientX === 0) {
                        scrollObj.next(e.clientX);
                    } else {
                        boardContainer.scrollLeft += scrollObj.next(e.clientX);
                    }        
            } else if((dragObject.element.getBoundingClientRect().left - 
                       boardContainer.offsetLeft) <= 25) {
                    if(scrollObj.clientX === 0) {
                        scrollObj.prev(e.clientX);
                    } else {
                        boardContainer.scrollLeft += scrollObj.prev(e.clientX);
                    }         
            }
        }
    }

    function mouseUpHandler(e) {
        if(dragObject.isDragging) {
            dragObject.element.style = dragObject.old;

            const elementUnderCursor = document.elementFromPoint(e.clientX, e.clientY);
            if(elementUnderCursor && elementUnderCursor.className.indexOf('board-section') !== -1) {
                const classContainer = elementUnderCursor.getElementsByClassName('card-container');
                if(classContainer && classContainer.length) {
                    classContainer[0].appendChild(dragObject.element);
                }
            } else {
                const closestSection = window.helpers.closest(
                    document.elementFromPoint(
                        e.clientX, e.clientY),
                        '.board-section');
                closestSection ?
                closestSection
                    .getElementsByClassName('card-container')[0]
                    .appendChild(dragObject.element) : 
                dragObject.parent.appendChild(dragObject.element);
            }
            dragObject.refresh();
        }
    }

    function dragHandler(e) {
    }

    function dragEndHandler() {
    }

    function dragOverHandler(e) {
    }

    Array.prototype.forEach.call(boardSections, sectionListener);

    function sectionListener(section) {
        
         const addButton = section.getElementsByClassName('board-add-button')[0];
 
         section.addEventListener('mouseover', (e) => {
             addButton.style.opacity = 1;
         });
 
         section.addEventListener('mouseout', (e) => {
             addButton.style.opacity = 0;
         });
 
         addButton.addEventListener('click', addButtonClickHandler);
 
         function addButtonClickHandler(e) {
             currentForm.clearForm();
 
             addButton.style.display="none";
             addButton.style.opacity=0;
             renderAddForm(section);
         }
    }

    function renderAddForm(section) {
        const createFormButton = section.getElementsByClassName('board-add-button')[0];
        const form = document.createElement('div');
        const inputField = document.createElement('textarea');
        const label = document.createElement('label');
        const addButton = document.createElement('button');
        const cancelButton = document.createElement('button');

        form.setAttribute('class', 'input-field col s12 add-card-block');
        
        inputField.setAttribute('name', 'title');
        inputField.setAttribute('class', 'materialize-textarea');
        inputField.setAttribute('type', 'text');

        label.setAttribute('for', 'title');
        label.appendChild(document.createTextNode('title'));
        
        addButton.setAttribute('class', 'waves-effect waves-light btn');
        addButton.style.margin = '3px';
        addButton.appendChild(document.createTextNode('Add'));
        addButton.addEventListener('click', addFormHandler);

        cancelButton.setAttribute('class', 'waves-effect waves-light btn');
        cancelButton.style.margin = '3px';
        cancelButton.appendChild(document.createTextNode('Cancel'));
        cancelButton.addEventListener('click', removeFormHandler);

        form.appendChild(inputField);
        form.appendChild(label);
        form.appendChild(addButton);
        form.appendChild(cancelButton);

        section.appendChild(form);

        currentForm.form = form;
        currentForm.section = section;
        currentForm.createButton = createFormButton;

        function removeFormHandler(e) {
            currentForm.clearForm();
        }

        function addFormHandler(e) {
            renderCard(section, inputField);
        }

        function renderCard(section, input) {
            const card = document.createElement('div');
            const content = document.createElement('div');
            const p = document.createElement('p');

            card.setAttribute('draggable', 'true');
            card.setAttribute('class', 'card z-depth-4 board-card dragable-card');

            p.appendChild(document.createTextNode(input.value));

            content.setAttribute('class', 'card-content');
            content.appendChild(p);

            card.appendChild(content);
            card.addEventListener('dragstart', dragStartHandler);
            card.addEventListener('mousedown', mouseDownHandler(card));
            card.addEventListener('mousemove', mouseMoveHandler);

            const cardContainer = section.getElementsByClassName('card-container')[0];

            cardContainer.appendChild(card);
            removeFormHandler();
        }
    }
}