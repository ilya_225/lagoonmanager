if(document.getElementById('create-board')) {
    document.body.style.overflowY="hidden";
    document.body.style.overflowX="hidden";
    const boardContainer = document.getElementById('create-board');
    const addButton = document.getElementById('add_section_button');
    const inputField = document.getElementById('section_name');
    const data = JSON.parse(boardContainer.getAttribute('data-json'));

    //TODO refactor scrollObj
    const scrollObj = {
        clientX : 0,
        shift: 0,
        next: function(x) {
            if(this.clientX !== 0)
                this.shift = x - this.clientX;
            else
                this.shift = 0;
            this.clientX = x;
            return 15;
        },
        prev: function(x) {
            if(this.clientX !== 0)
                this.shift = x - this.clientX;
            else
                this.shift = 0;
            this.clientX = x;
            return -15;
        }
    }

    const sectionObj = {
        sections: null,
        coords: [],
        sectionWidth: null,
        highlightedSection: null,
        highlightSection(x, y) {
            const filtered = this.coords.filter(el => {
                return x > el && x < (el + this.sectionWidth)
            });
            if(filtered.length) {
                const index = this.coords.indexOf(filtered[0]);
                if(this.highlightedSection)
                    this.highlightedSection.style.background = "#fff";
                this.highlightedSection = this.sections[index];
                this.highlightedSection.style.background="#e6ee9c";

            }
        },
        reset() {
            if(this.highlightedSection)
                this.highlightedSection.style.background = "#fff";
            this.highlightedSection = null;
            this.sectionWidth = null;
            this.sections = null;
            this.coords = [];
        },
        resetHighlightSection() {
            if(this.highlightedSection)
                this.highlightedSection.style.background = "#fff";
        }
    }
    
    updateContainer();

    addButton.addEventListener('click', (e) => {
        if(inputField.value.trim()) {
            data.push({
                'title' :inputField.value,
                position: data.length+1
            });
            updateContainer();
        }
    });

    function updateContainer() {
        window.helpers.emptyElement(boardContainer);
        renderCards(data);
        bindDragManager();
        calcSections();
    }

    function renderCards(data) {
        data.sort((a, b) => a.position - b.position)
        data.forEach((el, index) => {
            boardContainer.appendChild(generateCardComponent(el, index));
        });
    }

    function generateCardComponent(set, index) {
        const section = document.createElement('div');
        const card = document.createElement('div');
        const title = document.createElement('div');

        section.setAttribute('class', 'board-section');
        section.setAttribute('data-position', set.position);
        section.setAttribute('data-index', index);
        card.setAttribute('class', 'card z-depth-4 board-card');
        title.setAttribute('class', 'user-story-card-title custom-card-title');
        title.appendChild(document.createTextNode(set.title));

        section.appendChild(card);
        card.appendChild(title);

        return section;
    }

    function bindDragManager() {
        const boardCards = document.getElementsByClassName('board-card');
        const dragManager = new window.helpers.DragManager(boardCards, {
            mouseDownHandler: e => dragManager.dragObject,
            mouseMoveHandler: e => {           
                if(dragManager.dragObject.isDragging) {
                    
                    sectionObj.highlightSection(e.clientX + boardContainer.scrollLeft, e.clientY + boardContainer.scrollTop);
                    if(dragManager.dragObject.element.getBoundingClientRect().left >= 
                    (boardContainer.offsetWidth)) {
                        if(scrollObj.clientX === 0) {
                            scrollObj.next(e.clientX);
                        } else {
                            boardContainer.scrollLeft += scrollObj.next(e.clientX);
                        }        
                    } else if((dragManager.dragObject.element.getBoundingClientRect().left - 
                            boardContainer.offsetLeft) <= 25) {
                            if(scrollObj.clientX === 0) {
                                scrollObj.prev(e.clientX);
                            } else {
                                boardContainer.scrollLeft += scrollObj.prev(e.clientX);
                            }         
                    }
                }
            },
            mouseUpHandler  : e => {
                if(dragManager.dragObject.isDragging) {
                    sectionObj.resetHighlightSection();
                    dragManager.dragObject.element.style = dragManager.dragObject.old;
                    const elementUnderCursor = document.elementFromPoint(e.clientX, e.clientY);
                    const parentIndex = dragManager.dragObject.parent.getAttribute('data-index');
                    const parentPosition = dragManager.dragObject.parent.getAttribute('data-position');
                    dragManager.dragObject.parent.appendChild(dragManager.dragObject.element);

                    if(elementUnderCursor && elementUnderCursor.className.indexOf('board-section') !== -1) {
                        var targetIndex = elementUnderCursor.getAttribute('data-index');
                        var targetPosition = elementUnderCursor.getAttribute('data-position');
                        
                    } else {
                        const closestSection = window.helpers.closest(
                            elementUnderCursor, '.board-section');
                        if(closestSection) {
                            var targetIndex = closestSection.getAttribute('data-index');
                            var targetPosition = closestSection.getAttribute('data-position');
                        }
                    }

                    if(targetIndex && parentIndex) {
                        data[targetIndex].position = 1*parentPosition;
                        data[parentIndex].position = 1*targetPosition;
                    }
                    
                    updateContainer();
                    dragManager.dragObject.refresh();
                        
                }
            },
            defaultHandlers : true
        });
    }

    function calcSections() {
        const boardSections = document.getElementsByClassName('board-section');
        if(boardSections.length) {
            sectionObj.sectionWidth = boardSections[0].offsetWidth;
            const sectionCoords = Array.prototype.map.call(boardSections, el => {
                return el.getBoundingClientRect().left;
            });
            sectionObj.sections = boardSections;
            sectionObj.coords   =  sectionCoords;
        }
        
    }
}