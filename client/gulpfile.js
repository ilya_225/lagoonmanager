const gulp = require('gulp');
const css =  require('gulp-csso');
const uglify = require('gulp-uglify');
const pug = require('gulp-pug');
const webpack = require('webpack-stream');
const ejs = require("gulp-ejs");
const sass = require('gulp-sass');
const concat = require('gulp-concat');

gulp.task('styles', () => {
    return gulp.src('src/styles/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(css())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('dist/static/css'));
});

gulp.task('js', () => {
    return gulp.src('src/app/js/*.js')
        .pipe(concat('main.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('dist/static/js'));
});

gulp.task('webpack', () => {
    return gulp.src('src/app/js/*.js')
        .pipe(webpack(require ('./webpack.config.js')))
        .pipe(gulp.dest('dist/static/js'));
});

gulp.task('views', function buildHTML() {
  return gulp.src('src/views/*.ejs')
  .pipe(ejs({}, {}, { ext: '.html'}))
  .pipe(gulp.dest('dist/templates'));
});

gulp.task('watch', () => {
    gulp.watch('src/styles/*.scss',['styles']);
    gulp.watch('src/app/js/*.js',['js']);
    gulp.watch('src/views/*.ejs',['views']);
    gulp.watch('src/views/**/*.ejs',['views']);
});

gulp.task('materialize', () => {
  return gulp.src('src/materialize/materialize.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(css())
    .pipe(gulp.dest('dist/static/css'));
});

gulp.task('default', ['watch', 'styles', 'js', 'views', 'webpack']);

gulp.task('build', ['styles', 'js', 'views', 'webpack']);


