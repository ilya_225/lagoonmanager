buildscript {
	ext {
		springBootVersion = '1.5.3.RELEASE'
		postgresqlVersion = '9.4.1212'
	}
	repositories {
		mavenCentral()
	}
	dependencies {
		classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
	}
}

apply plugin: 'java'
apply plugin: 'eclipse'
apply plugin: 'idea'
apply plugin: 'org.springframework.boot'

version = '0.0.1-SNAPSHOT'
sourceCompatibility = 1.8

repositories {
	mavenCentral()
}

dependencies {
	compile("org.springframework.boot:spring-boot-starter-aop:${springBootVersion}")
	compile("org.springframework.boot:spring-boot-starter-data-jpa:${springBootVersion}")
	compile("org.springframework.boot:spring-boot-starter-freemarker:${springBootVersion}")
	compile('org.liquibase:liquibase-core:3.5.3')
	compile('org.liquibase.ext:liquibase-hibernate5:3.6')
	compile('org.hibernate:hibernate-validator:5.4.1.Final')
	compile('org.springframework.session:spring-session:1.3.1.RELEASE')
	compile("org.springframework.boot:spring-boot-starter-web-services:${springBootVersion}")
	compile('org.springframework.security:spring-security-test:4.2.2.RELEASE')
	compile("org.springframework.boot:spring-boot-starter-security:${springBootVersion}")
	compile("org.springframework.boot:spring-boot-starter-validation:${springBootVersion}")
	compile("org.springframework.boot:spring-boot-starter-web:${springBootVersion}")
	runtime("org.springframework.boot:spring-boot-devtools:${springBootVersion}")
	runtime('org.postgresql:postgresql:9.4.1212')
	compileOnly('org.projectlombok:lombok')
	testCompile("org.springframework.boot:spring-boot-starter-test:${springBootVersion}")
}
