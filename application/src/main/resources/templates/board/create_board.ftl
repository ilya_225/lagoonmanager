<#include "../dashboard/header.ftl">


    <div class="section-creator">
        <div class="row">
            <div class="input-field col s4">
                <input id="section_name" type="text">
                <label for="section_name">Section</label>
            </div>
            <button id="add_section_button" class="btn-floating btn-large waves-effect waves-light">
                <i class="material-icons">add</i>
            </button>
        </div>
    </div>

    <div class="board-container" id="create-board" data-json='[{"title":"ToDo","position":1},{"title":"In Progress","position":2},{"title":"Done","position":4},{"title":"Testitg", "position":3}]'>

    </div>

<#include "../dashboard/footer.ftl">