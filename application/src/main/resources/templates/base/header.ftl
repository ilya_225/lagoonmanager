<#include "_head.ftl">
<nav class="nav-extended">
    <div class="nav-wrapper ">
        <a href="#" class="brand-logo menu-logo">
        <span class="small__logo">
          Lagoon Manager
        </span>
        </a>
        <a href="#" class="left-align dropdown-button hide-on-large-only" data-activates="nav-mobile"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="dropdown-content">
            <li><a href="/home">Home</a></li>
            <li><a href="/login">Login</a></li>
            <li><a href="/register">Register</a></li>
        </ul>
        <ul class="right hide-on-med-and-down" id="mobile-demo">
            <li><a href="/home">Home</a></li>
            <li><a href="/login">Login</a></li>
            <li><a href="/register">Register</a></li>
        </ul>
    </div>
</nav>