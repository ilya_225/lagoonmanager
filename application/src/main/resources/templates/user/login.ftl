<#include "../base/header.ftl" >
<div class="row">
    <form action="/login" method="post" class="col s12 m6 offset-m3 l4 offset-l4">
        <div class="row">
            <div class="input-field col s12">
                <input id="username" name="username" type="text" class="validate">
                <label for="username">Username</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="password" name="password" type="password" class="validate">
                <label for="password">Password</label>
            </div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <#if RequestParameters.error?exists>
            <div class="validation_error">
                Invalid username and password
            </div>
        </#if>

        <button class="btn waves-effect waves-light" type="submit" name="action">Login
        </button>

    </form>
</div>

<#include "../base/footer.ftl">
