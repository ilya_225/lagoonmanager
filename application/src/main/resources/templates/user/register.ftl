<#include "../base/header.ftl">
<#import "/spring.ftl" as spring/>

<@spring.bind "user" />

<div class="row">
    <form action="/register" method="post" class="col s12 m5 offset-m4 l4 offset-l4">
        <div class="row">
            <div class="input-field col s12">
                <@spring.formInput "user.username" "name=\"username\" id=\"username\" type=\"text\" class=\"validate\"" />
                <label for="username">Username</label>
            <#list spring.status.errorMessages as error>

                <span class="validation_error">${error}</span>
            </#list>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <@spring.formInput "user.email" "name=\"email\" id=\"email\" type=\"text\" class=\"validate\"" />
                <label for="email">Email</label>
            <#list spring.status.errorMessages as error>
                <span class="validation_error">${error}</span>
            </#list>
            </div>
        </div>

    <div class="row">
    <div class="input-field col s12">
        <@spring.formPasswordInput "user.password","id=\"password\" class=\"validate\"" />
        <label for="password">Password</label>
    <#list spring.status.errorMessages as error>
        <span class="validation_error">${error}</span>
    </#list>
    </div>
    </div>

        <div class="row">
            <div class="input-field col s12">
                <input name="confirm" id="confirm" type="password" class="validate">
                <label for="confirm">Confirm</label>
            <#list spring.status.errorMessages as error>
                <span class="validation_error">${error}</span>
            </#list>
            </div>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

        <button class="btn waves-effect waves-light" type="submit" name="action">Sign-Up
        </button>

    </form>
</div>

<#include "../base/footer.ftl">