<#include "../dashboard/header.ftl">

<div class="row">

    <ul class="tabs">
        <li class="tab"><a href="#user_stories">List</a></li>
        <li class="tab"><a href="#create_story">Create</a></li>
    </ul>

    <div id="user_stories">
        <div class="row">
        <#list userStories as userStory >
            <div class="col s12 l4 m6">
                <div class="card z-depth-4">
                    <div class="user-story-card-title"><a href="#">${userStory.getName()}</a></div>
                    <div class="card-content">
                        ${userStory.getBody()}
                    </div>
                    <div class="card-action">
                        <a href="#">
                            Preview
                        </a>
                    </div>
                </div>

            </div>
        </#list>

        </div>
    </div>

    <div id="create_story" >
        <div class="row">
            <form action="/project/${project.getId()}/user-stories/create" method="POST" class="col s12" >
                <div class="row">
                    <div class="input-field col s12">
                        <input id="story_title" name="name" type="text" class="validate">
                        <label for="story_title">Story Title</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <textarea name="body" id="editor" name="editor" rows="20" columns="6"> </textarea>
                    </div>
                </div>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                </button>
            </form>

        </div>
    </div>
</div>

<#include "../dashboard/footer.ftl" />