<#include "header.ftl">

<div class="row" >

    <h3>Dashboard</h3>

    <div>
        <div>Username: <span> ${user.getUsername()}</span></div>
        <div>Email: <span> ${user.getEmail()}</span></div>
        <div>Active: <span> <#if user.isEnabled() > Enabled <#else> Disabled </#if></span></div>
    </div>
</div>

<#include "footer.ftl">
