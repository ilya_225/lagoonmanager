<#include "../base/_head.ftl">
<#import "/spring.ftl" as spring/>

<header>
    <div class="row">
        <nav>
            <div class="nav-wrapper">

                <a href="#" class="brand-logo menu-logo" data-activates="drop_mobile"><span class="small__logo">
       Lagoon Manager
      </span></a>
                <a href="#" class="left-align dropdown-button hide-on-large-only" data-activates="drop_mobile"><i class="material-icons">more_vert</i></a>

                <ul id="drop_mobile" class="dropdown-content">

                <#list menu?keys as menuTitle>
                    <li><a class="center-align" href="${menu[menuTitle]}">${menuTitle}</a></li>
                </#list>

                </ul>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="dashboard.html">Dashbord</a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="drop_projects">Projects<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a href="#">Settings</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <ul id="drop_projects" class="dropdown-content">
        <li><a href="#!">one</a></li>
        <li><a href="#!">two</a></li>
        <li class="divider"></li>
        <li><a href="projects.html">all</a></li>
    </ul>
</header>



<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="userView">
            <div class="background">
                <img src="/images/1.jpg">
            </div>
            <a href="#!user"><img class="circle" src="/images/no-avatar.png"></a>
            <a href="#!name"><span class="white-text name">${user.username}</span></a>
            <a href="#!email"><span class="white-text email">${user.email}</span></a>
        </div>
    </li>

    <#list menu?keys as menuTitle>
        <li><a class="waves-effect" href="${menu[menuTitle]}">${menuTitle}</a></li>
    </#list>

</ul>
<main>