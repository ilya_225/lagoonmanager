</main>
<script src="https://use.fontawesome.com/1a45d0a437.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/js/materialize.min.js"></script>
<script src="/js/ckeditor/ckeditor.js"></script>
<script src="/js/bundle.js"></script>
<script src="/js/jquery.materialize-autocomplete.js"></script>
<script>
    $(document).ready(function() {
        $('select').material_select();
        $('.tooltipped').tooltip({delay: 50});
        $('.modal').modal();

        var multiple = $('#multipleInput').materialize_autocomplete({
            multiple: {
                enable: true
            },
            appender: {
                el: '.ac-users'
            },
            dropdown: {
                el: '#multipleDropdown'
            }
        });

        var resultCache = {
            'A': [
                {
                    id: 'Abe',
                    text: 'Abe'
                },
                {
                    id: 'Ari',
                    text: 'Ari'
                }
            ],
            'B': [
                {
                    id: 'Baz',
                    text: 'Baz'
                }
            ],
            'BA': [
                {
                    id: 'Baz',
                    text: 'Baz'
                }
            ],
            'BAZ': [
                {
                    id: 'Baz',
                    text: 'Baz'
                }
            ],
            'AB': [
                {
                    id: 'Abe',
                    text: 'Abe'
                }
            ],
            'ABE': [
                {
                    id: 'Abe',
                    text: 'Abe'
                }
            ],
            'AR': [
                {
                    id: 'Ari',
                    text: 'Ari'
                }
            ],
            'ARI': [
                {
                    id: 'Ari',
                    text: 'Ari'
                }
            ]
        };
        multiple.resultCache = resultCache;
    });

</script>
</body>
</html>