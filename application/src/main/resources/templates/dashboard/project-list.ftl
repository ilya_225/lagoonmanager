<#include "../dashboard/header.ftl">




<#list projects_list as project >

<div class="row">
    <div class="col s12 m12">
        <div class="card">
            <div class="card-content">
                <span class="card-title"><a class="project_link" href="/project/${project.getId()}" >${project.getName()}</a></span>
                <div class="chip">Java</div>
                <div class="chip">JS</div>
                <div class="chip">React</div>
                <div class="chip">Spring</div>
                <table class="project_table">

                    <tbody>
                    <tr>
                        <td >Members</td>
                        <td>1</td>
                    </tr>

                    <tr>
                        <td>Tasks</td>
                        <td>10</td>
                    </tr>

                    <tr>
                        <td >Users stories</td>
                        <td>10</td>
                    </tr>

                    <tr>
                        <td >Bugs</td>
                        <td>0</td>
                    </tr>

                    <tr>
                        <td >Worked</td>
                        <td>10h</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

</#list>


<#include "../dashboard/footer.ftl" />