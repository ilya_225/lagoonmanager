<#include "../dashboard/header.ftl">

<div class="row">
    <form action="/dashboard/create-project/" method="post" class="col s12">

        <div class="row">
            <div class="input-field col l6 s12">
                <input placeholder="Placeholder" name="title" id="title" type="text" class="validate">
                <label for="title">Name</label>

            </div>
        </div>
        <p>
            <input type="checkbox" name="visibility" id="visibility" />
            <label for="visibility">Public Project</label>
        </p>

        <div class="row">
            <div class="input-field col l6 s12">
                <textarea id="description" name="description" class="materialize-textarea"></textarea>
                <label for="description">Description</label>
            </div>
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button class="btn waves-effect waves-light" type="submit">Submit
            <i class="material-icons right">send</i>
        </button>
    </form>
</div>

<#include "../dashboard/footer.ftl" />