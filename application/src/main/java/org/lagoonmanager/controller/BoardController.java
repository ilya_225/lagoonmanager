package org.lagoonmanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping("/board")
public class BoardController {

    @GetMapping("/index")
    public String index(/*@PathVariable Long id,*/ Model model) {
        model.addAttribute("menu", addMenu());

        return "board/board";
    }

    @GetMapping("/create_board")
    public String createBoard(/*@PathVariable Long id,*/ Model model) {
        model.addAttribute("menu", addMenu());

        return "board/create_board";
    }

    private Map<String, String> addMenu() {
        Map<String, String> menu = new LinkedHashMap<>();
        menu.put("Boards", "#");
        menu.put("Create board", "#");

        return menu;
    }
}
