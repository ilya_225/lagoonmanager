package org.lagoonmanager.controller;

import org.lagoonmanager.entity.Project;
import org.lagoonmanager.form.ProjectForm;
import org.lagoonmanager.service.ProjectService;
import org.lagoonmanager.service.WebFormFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {

    private ProjectService projectService;
    private WebFormFactory webFormFactory;

    public DashboardController(ProjectService projectService, WebFormFactory webFormFactory) {
        this.projectService = projectService;
        this.webFormFactory = webFormFactory;
    }

    @ModelAttribute("title")
    public String addTitle() {
        return "Dashboard";
    }

    @ModelAttribute("menu")
    public Map<String, String> addMenu() {
        Map<String, String> menu = new LinkedHashMap<>();
        menu.put("Home", "/dashboard");
        menu.put("Projects", "/dashboard/project-list");
        menu.put("Create Project", "/dashboard/create-project");
        menu.put("User Profile", "#");
        /*menu.put("Github Accounts", "#");
        menu.put("Settings", "#");
        menu.put("Wiki", "#");*/

        return menu;
    }

    @GetMapping
    public String index(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            model.addAttribute("user", principal);
        }
        return "dashboard/index";
    }

    @GetMapping("/create-project")
    public String createProject(Model model) {

        model.addAttribute("project_form", webFormFactory.createProjectForm());
        return "dashboard/create-project";
    }

    @PostMapping("/create-project")
    public String createProjectPost(@ModelAttribute("project_form") @Valid ProjectForm projectForm) {

        Project project = projectService.saveProject(projectForm);

        return "redirect:/dashboard";
    }

    @GetMapping("/project-list")
    public String listProjects(Model model) {

        List<Project> projectList = projectService.findByOwner();
        model.addAttribute("projects_list", projectList);

        return "dashboard/project-list";
    }
}
