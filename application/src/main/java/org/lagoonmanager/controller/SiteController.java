package org.lagoonmanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SiteController {

    @GetMapping({"/","/home"})
    public String homePage(Model model) {
        model.addAttribute("title", "Home");
        return "/home";
    }

}
