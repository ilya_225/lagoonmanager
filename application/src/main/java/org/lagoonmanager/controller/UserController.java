package org.lagoonmanager.controller;

import org.lagoonmanager.entity.User;
import org.lagoonmanager.form.UserForm;
import org.lagoonmanager.service.SecurityService;
import org.lagoonmanager.service.UserService;
import org.lagoonmanager.validator.PasswordValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@Validated
public class UserController {

    private PasswordValidator passwordValidator;
    private UserService userService;
    private SecurityService securityService;

    public UserController(PasswordValidator passwordValidator,
                          UserService userService,
                          SecurityService securityService) {
        this.passwordValidator = passwordValidator;
        this.userService = userService;
        this.securityService = securityService;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(passwordValidator);
    }

    @GetMapping("/register")
    public String registerUser(Model model) {
        model.addAttribute("user", new UserForm());
        model.addAttribute("title", "Register");
        return "user/register";
    }

    @PostMapping("/register")
    public String registerSubmit(@ModelAttribute("user") @Valid UserForm userForm,
                                 BindingResult bindingResult,
                                 Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", userForm);
            return "user/register";
        }
        userService.save(userForm);
        securityService.autoLogin(userForm.getUsername(), userForm.getConfirm());

        return "redirect:/dashboard";
    }

    @GetMapping("/login")
    public String loginUser(Model model) {
        model.addAttribute("title", "Login");
        return "user/login";
    }

    @RequestMapping("/user/search")
    public @ResponseBody List<User> searchUser() {
        return userService.findUsersByUsername();
    }
}
