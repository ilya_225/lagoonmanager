package org.lagoonmanager.controller;

import org.lagoonmanager.entity.Project;
import org.lagoonmanager.entity.UserStory;
import org.lagoonmanager.service.ProjectService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@Validated
@RequestMapping("/project")
public class ProjectController {

    private ProjectService projectService;

    @ModelAttribute("title")
    public String addTitle() {
        return "Project";
    }

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/{id}/general-info")
    public String projectGeneralInfoPage(@PathVariable Long id, Model model) {
        Project project = projectService.findProject(id);

        model.addAttribute("menu", addMenu(project));

        return "project_general_info/index";
    }

    @GetMapping("/{id}")
    public String editProject(@PathVariable Long id, Model model) {
        Project project = projectService.findProject(id);

        model.addAttribute("project", project);
        model.addAttribute("menu", addMenu(project));

        return "project/project";
    }

    @GetMapping("/{id}/user-stories")
    public String userStories(@PathVariable Long id, Model model) {
        Project project = projectService.findProject(id);

        model.addAttribute("project", project);
        model.addAttribute("userStories", projectService.findAllUserStories());
        model.addAttribute("menu", addMenu(project));

        return "project/user-stories";
    }

    @PostMapping("/{id}/user-stories/create")
    public String createUserStory(@PathVariable Long id,
                                  @ModelAttribute("user_story")
                                  @Valid UserStory userStory) {

        Project project = projectService.findProject(id);
        userStory.setProject(project);
        projectService.saveUserStory(userStory);

        return "redirect:/project/" + project.getId() + "/user-stories";
    }

    private Map<String, String> addMenu(Project project) {
        Map<String, String> menu = new LinkedHashMap<>();
        menu.put("Project General Info", "/project/" + project.getId() + "/general-info");
        menu.put("Boards", "/board/index");
        menu.put("Activity", "#");
        menu.put("User Stories", "/project/" + project.getId() + "/user-stories");
        menu.put("Issues", "#");
        menu.put("Members", "#");
        menu.put("Roles", "#");

        return menu;
    }
}
