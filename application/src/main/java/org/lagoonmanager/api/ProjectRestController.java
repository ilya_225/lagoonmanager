package org.lagoonmanager.api;

import org.hibernate.Hibernate;
import org.lagoonmanager.entity.Project;
import org.lagoonmanager.entity.User;
import org.lagoonmanager.exception.NotFoundException;
import org.lagoonmanager.service.ProjectService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/project")
public class ProjectRestController {

    private ProjectService projectService;

    public ProjectRestController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @RequestMapping(method = { RequestMethod.GET }, path = "/all")
    public List<Project> getAllProjects() {
        return projectService.findByOwner();
    }
}
