package org.lagoonmanager.security.voter;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;

import java.util.Collection;

public class ProjectVoter implements AccessDecisionVoter {

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class aClass) {
        return true;
    }

    @Override
    public int vote(Authentication authentication, Object o, Collection collection) {

        System.out.println(o);
        System.out.println(collection);
        return ACCESS_GRANTED;
    }
}
