package org.lagoonmanager.security;

import org.lagoonmanager.entity.User;
import org.lagoonmanager.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class ProjectSecurity {

    private ProjectService projectService;

    @Autowired
    public ProjectSecurity(ProjectService projectService) {
        this.projectService = projectService;
    }

    public boolean isOwner(Authentication authentication, String slug) {
        System.out.println(authentication);
        System.out.println(slug);

        Long id;
        try {
            id = Long.valueOf(slug);
            Object userObj = authentication.getPrincipal();

            if (userObj instanceof User) {
                User user = (User) userObj;
                Integer count = projectService.countUserProject(user, id);
                if (count > 0) {
                    return true;
                }
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return false;
    }

}
