package org.lagoonmanager.utils;

import org.lagoonmanager.exception.BadRequestException;
import org.lagoonmanager.exception.NotFoundException;
import org.lagoonmanager.exception.RepositoryEmptyResultException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice("org.lagoonmanager.controller")
public class CommonControllerAdvice {

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public String notFoundHandler() {

        return "error/404";
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(RepositoryEmptyResultException.class)
    public String repositoryEmptyResultHandler(RepositoryEmptyResultException e) {

        return "error/404";
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public String accessDeniedHandler(AccessDeniedException e) {

        return "error/403";
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    public String badRequestHandler(BadRequestException e) {
            return "error/400";
        }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public String implicitBadRequestHandler(MethodArgumentTypeMismatchException e) {
        return "error/400";
    }

}
