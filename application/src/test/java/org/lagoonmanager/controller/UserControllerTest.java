package org.lagoonmanager.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lagoonmanager.config.WebSecurityConfig;
import org.lagoonmanager.security.ProjectSecurity;
import org.lagoonmanager.repository.BoardTypeRepository;
import org.lagoonmanager.repository.ProjectRepository;
import org.lagoonmanager.repository.UserRepository;
import org.lagoonmanager.service.ProjectService;
import org.lagoonmanager.service.SecurityService;
import org.lagoonmanager.service.UserService;
import org.lagoonmanager.service.impl.UserDetailsServiceImpl;
import org.lagoonmanager.validator.PasswordValidator;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@Import(WebSecurityConfig.class)
public class UserControllerTest {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PasswordValidator passwordValidator;

    @MockBean
    private ProjectSecurity projectSecurity;

    @MockBean
    private BoardTypeRepository boardTypeRepository;

    @MockBean
    private ProjectRepository projectRepository;

    @MockBean
    private ProjectService projectService;

    @MockBean
    private UserService userService;

    @MockBean
    private SecurityService securityService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @Before public void initialize() {
        Mockito.when(passwordValidator.supports(Matchers.any())).thenReturn(true);
    }


    @Test
    public void testRegisterUser() throws Exception {
        this.mvc.perform(get("/register"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(view().name("user/register"));
    }

    @Test
    public void testRegisterSubmit() throws Exception {
        this.mvc.perform(post("/register")
                .param("username", "JohnDoe")
                .param("password", "qwerty1111")
                .param("confirm", "qwerty1111")
                .param("email", "test@test.test")
                .with(csrf()))
                .andExpect(status().is3xxRedirection());
    }

}
